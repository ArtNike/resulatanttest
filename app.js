let express = require("express");
let app = express();
let request = require("request");
let file = require('fs');
let MongoClient = require('mongodb').MongoClient;
let url = "mongodb://localhost:27017/currency";
app.use(express.static("./public"));
app.set('views', 'views');
app.set('view engine', 'jade');
app.engine('jade', require('jade').__express);
app.get('/', (req, res) => {
    res.render('index');
});
let io = require('socket.io').listen(app.listen(3700));
console.log('Server started');
updateCurrency();
setInterval(updateCurrency, 15000);
io.sockets.on('connection', (socket) => {
    socket.on('getCurrency', () => {
        if (socket.timerId) {
            clearInterval(socket.timerId);
        }
        sendCurrencyList(socket);
        socket.timerId = setInterval(() => {
            sendCurrencyList(socket);
        }, 15000);

    });
    socket.on('disconnect', (reason) => {
        clearInterval(socket.timerId);
        socket.disconnect(true);
        console.log('closed');
    })
});

function sendCurrencyList(socket) {
    resp = file.readFile("./currency.json", 'utf8', (err, contents) => {
        if (!err) {
            let resp = [];
            contents = JSON.parse(contents);
            for (let i = 0; i < contents.stock.length; i++) {
                resp.push({name: contents.stock[i].name, volume: Math.round(contents.stock[i].volume), amount: contents.stock[i].price.amount.toFixed(2)});
            }
            socket.emit('currencyList', resp);
        }
    });
}

function updateCurrency() {
    request('http://phisix-api3.appspot.com/stocks.json', (error, response, body) => {
        if (!error) {
            console.log(body);
            let contents = file.writeFile("./currency.json", body,
                (error) => {
                    console.log("written file");
                }
            );
        }
    });

}

module.exports = app;
