window.onload = function () {
    console.log('amazing');
    let socket = io.connect('http://localhost:3700');
    console.log('connected');
    let button = document.getElementById('button');
    socket.on('currencyList', function (data) {
        let table = document.getElementById('table');
        table.innerHTML = "";
        console.log(data);
        let thName = document.createElement('th');
        thName.appendChild(document.createTextNode('Name'));
        let thVolume = document.createElement('th');
        thVolume.appendChild(document.createTextNode('Volume'));
        let thAmount = document.createElement('th');
        thAmount.appendChild(document.createTextNode('Amount'));
        table.appendChild(thName);
        table.appendChild(thVolume);
        table.appendChild(thAmount);
        for(let i = 0; i < data.length; i++){
            let tr = document.createElement('tr');
            tr.setAttribute('id', i.toString());
            let tdName = document.createElement('td');
            let tdVolume = document.createElement('td');
            let tdAmount = document.createElement('td');
            tdName.appendChild(document.createTextNode(data[i].name));
            tdVolume.appendChild(document.createTextNode(data[i].volume));
            tdAmount.appendChild(document.createTextNode(data[i].amount));
            tr.appendChild(tdName);
            tr.appendChild(tdVolume);
            tr.appendChild(tdAmount);
            table.appendChild(tr);
        }
    });
    socket.emit('getCurrency');
    button.onclick = ()=>{socket.emit('getCurrency');};
};